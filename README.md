# Setting up the project for local development (only done once to set up application)

1. # Clone project

   - 1. git clone https://gitlab.com/pvd232/my-weight-pal.git

2. # Creating local Postgres DB instance

   - 1. Enter into terminal "psql -U postgres"
   - 2. Enter into terminal "create database myweightpaldb"
   - 3. Verify database has been created by entering "\l" into the command prompt to view all databases

3. # Creating Python virtual environment and installing dependencies for Mac users

   - 1. Navigate to project root directory
   - 2. Enter into terminal "python3 -m venv venv"
   - 3. Enter into terminal "source ./venv/bin/activate"
   - 4. Enter into terminal "pip install -r requirements.txt"

4. # Setting up Flask app

   - 1. Navigate to project root directory
   - 2. Start flask server by entering into terminal "main"
   - 3. Navigate to "localhost:4000/start_up" in the browser to instantiate the data models in the database

5. # React app setup
   - 1. Navigate to "frontend-react-app" directory and enter into terminal "npm install"
   - 2. Enter into terminal "npm start"
   - 3. Go to your browser and navigate to localhost:3000

# Running the project locally (done each time to start application)

1. Navigate to project root directory in terminal

2. Activate virtual environment by entering into terminal "source ./venv/bin/activate"

3. Start flask server by entering into terminal "main"

4. Navigate to "frontend-react-app" directory in terminal and enter "npm start"
